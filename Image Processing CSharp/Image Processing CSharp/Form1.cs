﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Image_Processing_CSharp
{
    public partial class Form1 : Form
    {
        Bitmap imageFile, defaultFile;
        string fileName;
        int[] redCount = new int[256], blueCount = new int[256], greenCount = new int[256];
        float[,] filter, redFilter, greenFilter, blueFilter;
        int m, n, aFilter, bFilter;
        ComplexNumber[,] complex;
        ComplexNumber[,] complexR, complexG, complexB;

        public Form1()
        {
            fileName = "Midgardsormr.png";
            InitializeComponent();
            //defaultFile = new Bitmap("einstein.JPG", true);
            defaultFile = new Bitmap(fileName, true);
            imageFile = null;
            pictureBox2.Image = defaultFile;

            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            try
            {
                int x, y;

                // Loop through the images pixels to reset color.
                for (x = 0; x < imageFile.Width; x++)
                {
                    for (y = 0; y < imageFile.Height; y++)
                    {
                        Color pixelColor = imageFile.GetPixel(x, y);
                        //MessageBox.Show(pixelColor.R.ToString());
                    }
                }

                // Set the PictureBox to display the image.
                pictureBox3.Image = imageFile;

                // Display the pixel format in Label1.
                label1.Text = "Pixel format: " + imageFile.PixelFormat.ToString();

            }
            catch (ArgumentException)
            {
                MessageBox.Show("There was an error." +
                    "Check the path to the image file.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                imageFile = new Bitmap(fileName, true);

                int x, y, grayLevel, murkiplier;
                int r, g, b;

                grayLevel = int.Parse(textBox1.Text);
                murkiplier = (256 / grayLevel);

                // Loop through the images pixels to reset color.
                for (x = 0; x < imageFile.Width; x++)
                {
                    for (y = 0; y < imageFile.Height; y++)
                    {
                        if (true)
                        {
                            Color pixelColor = imageFile.GetPixel(x, y);

                            if (((int)(pixelColor.R / (256 / grayLevel))) == 0)
                            {
                                r = 0;
                            }
                            else if (((int)(pixelColor.R / (256 / grayLevel))) >= (grayLevel - 1))
                            {
                                r = 255;
                            }
                            else
                            {
                                r = ((int)(pixelColor.R / (256 / grayLevel))) * (256 / grayLevel) + (int)((256 / grayLevel) * 0.5);
                            }

                            if (((int)(pixelColor.G / (256 / grayLevel))) == 0)
                            {
                                g = 0;
                            }
                            else if (((int)(pixelColor.G / (256 / grayLevel))) >= (grayLevel - 1))
                            {
                                g = 255;
                            }
                            else
                            {
                                g = ((int)(pixelColor.G / (256 / grayLevel))) * (256 / grayLevel) + (int)((256 / grayLevel) * 0.5);
                            }

                            if (((int)(pixelColor.B / (256 / grayLevel))) == 0)
                            {
                                b = 0;
                            }
                            else if (((int)(pixelColor.B / (256 / grayLevel))) >= (grayLevel - 1))
                            {
                                b = 255;
                            }
                            else
                            {
                                b = ((int)(pixelColor.B / (256 / grayLevel))) * (256 / grayLevel) + (int)((256 / grayLevel) * 0.5);
                            }

                            Color newColor = Color.FromArgb((r + g + b) / 3, (r + g + b) / 3, (r + g + b) / 3);
                            imageFile.SetPixel(x, y, newColor);
                        }
                        //else
                        //{
                        //    Color pixelColor = imageFile.GetPixel(x, y);

                        //    r = 0; g = 0; b = 0;

                        //    if (pixelColor.R >= 128 || pixelColor.G >= 128 || pixelColor.B >= 128)
                        //    {
                        //        r = 255;
                        //        g = 255;
                        //        b = 255;
                        //    }

                        //    Color newColor = Color.FromArgb(r, g, b);
                        //    imageFile.SetPixel(x, y, newColor);
                        //}
                    }
                }

                // Set the PictureBox to display the image.
                pictureBox3.Image = imageFile;
            }
            catch
            {
                MessageBox.Show("Invalid Gray Level.");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int x, y, grayLevel, murkiplier;
                int r, g, b;

                grayLevel = int.Parse(textBox1.Text);
                murkiplier = (256 / grayLevel);

                // Loop through the images pixels to reset color.
                for (x = 0; x < imageFile.Width; x++)
                {
                    for (y = 0; y < imageFile.Height; y++)
                    {
                        Color pixelColor = imageFile.GetPixel(x, y);
                        if (pixelColor.R >= 128)
                        {
                            r = 255;
                        }
                        else
                        {
                            r = 0;
                        }

                        if (pixelColor.G >= 128)
                        {
                            g = 255;
                        }
                        else
                        {
                            g = 0;
                        }

                        if (pixelColor.B >= 128)
                        {
                            b = 255;
                        }
                        else
                        {
                            b = 0;
                        }
                        Color newColor = Color.FromArgb(r, g, b);
                        imageFile.SetPixel(x, y, newColor);
                        //MessageBox.Show(pixelColor.R.ToString());
                    }
                }

                // Set the PictureBox to display the image.
                pictureBox3.Image = imageFile;

                // Display the pixel format in Label1.
                label1.Text = "Pixel format: " + imageFile.PixelFormat.ToString();

            }
            catch (ArgumentException)
            {
                MessageBox.Show("There was an error." +
                    "Check the path to the image file.");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                float scale = float.Parse(textBox2.Text);
                imageFile = new Bitmap((int)(defaultFile.Width * scale), (int)(defaultFile.Height * scale));
                for (int x = 0; x < imageFile.Width; x++)
                {
                    for (int y = 0; y < imageFile.Height; y++)
                    {
                        Color newColor = Color.FromArgb(defaultFile.GetPixel((int)(x / scale), (int)(y / scale)).R, defaultFile.GetPixel((int)(x / scale), (int)(y / scale)).G, defaultFile.GetPixel((int)(x / scale), (int)(y / scale)).B);
                        imageFile.SetPixel(x, y, newColor);
                    }
                }
                pictureBox3.Image = imageFile;
            }
            catch
            {
                MessageBox.Show("Invalid Zoom Level.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);

            int x, y;
            int r, g, b;

            // Loop through the images pixels to reset color.
            for (x = 0; x < imageFile.Width; x++)
            {
                for (y = 0; y < imageFile.Height; y++)
                {
                    Color pixelColor = imageFile.GetPixel(x, y);

                    r = 255 - pixelColor.R;
                    g = 255 - pixelColor.G;
                    b = 255 - pixelColor.B;

                    Color newColor = Color.FromArgb(r, g, b);
                    imageFile.SetPixel(x, y, newColor);
                    //MessageBox.Show(pixelColor.R.ToString());
                }
            }

            // Set the PictureBox to display the image.
            pictureBox3.Image = imageFile;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                imageFile = new Bitmap(fileName, true);
                float c = float.Parse(textBox3.Text);

                float max = (float)(c * Math.Log10((256.0f)));


                for (int x = 0; x < imageFile.Width; x++)
                {
                    for (int y = 0; y < imageFile.Height; y++)
                    {
                        float r, g, b;
                        Color pixelColor = imageFile.GetPixel(x, y);

                        r = (float)((c * Math.Log10((double)(1 + pixelColor.R))) / max) * 255.0f;
                        g = (float)((c * Math.Log10((double)(1 + pixelColor.G))) / max) * 255.0f;
                        b = (float)((c * Math.Log10((double)(1 + pixelColor.B))) / max) * 255.0f;

                        Color newColor = Color.FromArgb((int)r, (int)g, (int)b);
                        imageFile.SetPixel(x, y, newColor);
                        //MessageBox.Show(pixelColor.R.ToString());
                    }
                }
                pictureBox3.Image = imageFile;
            }
            catch
            {
                MessageBox.Show("Invalid C Value.");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                float scale = float.Parse(textBox2.Text);
                imageFile = new Bitmap((int)(defaultFile.Width * scale), (int)(defaultFile.Height * scale));
                //for (float x = 0; x < imageFile.Width; x += scale)
                //{
                //    for (float y = 0; y < imageFile.Height; y += scale)
                //    {
                //        Color newColor = defaultFile.GetPixel((int)(x / scale), (int)(y / scale));
                //        imageFile.SetPixel((int)x, (int)y, newColor);
                //    }
                //}

                for (int x = 0; x < imageFile.Width; x++)
                {
                    for (int y = 0; y < imageFile.Height; y++)
                    {
                        Color newColor = Color.FromArgb(defaultFile.GetPixel((int)(x / scale), (int)(y / scale)).R, defaultFile.GetPixel((int)(x / scale), (int)(y / scale)).G, defaultFile.GetPixel((int)(x / scale), (int)(y / scale)).B);
                        imageFile.SetPixel(x, y, newColor);
                    }
                }

                for (int x = 0; x < imageFile.Width; x++)
                {
                    for (int y = 0; y < imageFile.Height; y++)
                    {
                        int r, g, b, x1, x2, y1, y2;

                        x1 = (int)((int)(x / scale) * scale);
                        x2 = (int)(((int)(x / scale) + 1) * scale);

                        y1 = (int)((int)(y / scale) * scale);
                        y2 = (int)(((int)(y / scale) + 1) * scale);

                        x2 = (x2 >= imageFile.Width) ? (imageFile.Width - 1) : x2;
                        y2 = (y2 >= imageFile.Height) ? (imageFile.Height - 1) : y2;

                        Color color11 = imageFile.GetPixel(x1, y1), color12 = imageFile.GetPixel(x1, y2), color21 = imageFile.GetPixel(x2, y1), color22 = imageFile.GetPixel(x2, y2);

                        int divider = ((x2 - x1) * (y2 - y1));
                        int difx1 = (x - x1), difx2 = (x2 - x), dify1 = (y - y1), dify2 = (y2 - y);

                        r = (divider == 0) ? color11.R : (color11.R * difx2 * dify2 + color21.R * difx1 * dify2 + color12.R * difx2 * dify1 + color22.R * difx1 * dify1) / divider;
                        g = (divider == 0) ? color11.G : (color11.G * difx2 * dify2 + color21.G * difx1 * dify2 + color12.G * difx2 * dify1 + color22.G * difx1 * dify1) / divider;
                        b = (divider == 0) ? color11.B : (color11.B * difx2 * dify2 + color21.B * difx1 * dify2 + color12.B * difx2 * dify1 + color22.B * difx1 * dify1) / divider;

                        Color newColor = Color.FromArgb(r, g, b);
                        imageFile.SetPixel(x, y, newColor);
                    }
                }

                pictureBox3.Image = imageFile;
            }
            catch
            {
                MessageBox.Show("Invalid Zoom Level.");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                float scale = float.Parse(textBox2.Text);
                imageFile = new Bitmap((int)(defaultFile.Width * scale), (int)(defaultFile.Height * scale));
                for (float x = 0; x < imageFile.Width; x += scale)
                {
                    for (float y = 0; y < imageFile.Height; y += scale)
                    {
                        Color newColor = defaultFile.GetPixel((int)(x / scale), (int)(y / scale));
                        imageFile.SetPixel((int)x, (int)y, newColor);
                    }
                }
                //MessageBox.Show(imageFile.Height.ToString());
                for (int x = 0; x < imageFile.Width; x++)
                {
                    for (int y = 0; y < imageFile.Height; y++)
                    {
                        int r, g, b, x1, x2, y1, y2;

                        x1 = (int)((int)(x / scale) * scale);
                        x2 = (int)(((int)(x / scale) + 1) * scale);

                        y1 = (int)((int)(y / scale) * scale);
                        y2 = (int)(((int)(y / scale) + 1) * scale);

                        x2 = (x2 >= imageFile.Width) ? (imageFile.Width - 1) : x2;
                        y2 = (y2 >= imageFile.Height) ? (imageFile.Height - 1) : y2;

                        try
                        {
                            Color color11 = imageFile.GetPixel(x1, y1), color12 = imageFile.GetPixel(x1, y2), color21 = imageFile.GetPixel(x2, y1), color22 = imageFile.GetPixel(x2, y2);

                            int divider = ((x2 - x1) * (y2 - y1));
                            int difx1 = ((x - x1) <= 0) ? 1 : (x - x1), difx2 = ((x2 - x) <= 0) ? 1 : (x2 - x), dify1 = ((y - y1) <= 0) ? 1 : (y - y1), dify2 = ((y2 - y) <= 0) ? 1 : (y2 - y);

                            divider = (divider == 0) ? 1 : divider;

                            r = (color11.R * difx2 * dify2 + color21.R * difx1 * dify2 + color12.R * difx2 * dify1 + color22.R * difx1 * dify1) / divider;
                            g = (color11.G * difx2 * dify2 + color21.G * difx1 * dify2 + color12.G * difx2 * dify1 + color22.G * difx1 * dify1) / divider;
                            b = (color11.B * difx2 * dify2 + color21.B * difx1 * dify2 + color12.B * difx2 * dify1 + color22.B * difx1 * dify1) / divider;

                            r = (r == 0) ? color11.R : r;
                            g = (g == 0) ? color11.G : g;
                            b = (b == 0) ? color11.B : b;

                            Color newColor = Color.FromArgb(r, g, b);
                            imageFile.SetPixel(x, y, newColor);
                        }
                        catch (Exception)
                        {
                            //MessageBox.Show(x1 + ", " + y1 + " | " + x2 + ", " + y2);
                        }
                    }
                }
                pictureBox3.Image = imageFile;
            }
            catch
            {
                MessageBox.Show("Invalid Zoom Level.");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                float c = float.Parse(textBox3.Text);
                float gamma = float.Parse(textBox4.Text);

                float max = (float)(c * Math.Pow(255.0, gamma));

                imageFile = new Bitmap(fileName, true);

                for (int x = 0; x < imageFile.Width; x++)
                {
                    for (int y = 0; y < imageFile.Height; y++)
                    {
                        float r, g, b;

                        r = (float)((c * Math.Pow(imageFile.GetPixel(x, y).R, gamma) / max) * 255.0f);
                        g = (float)((c * Math.Pow(imageFile.GetPixel(x, y).G, gamma) / max) * 255.0f);
                        b = (float)((c * Math.Pow(imageFile.GetPixel(x, y).B, gamma) / max) * 255.0f);

                        Color newColor = Color.FromArgb((int)r, (int)g, (int)b);

                        imageFile.SetPixel(x, y, newColor);
                    }
                }

                pictureBox3.Image = imageFile;
            }
            catch
            {
                MessageBox.Show("Invalid C Value or Gamma.");
            }
        }

        private void ContrastPoint()
        {
            float x1 = trackBar1.Value, y1 = trackBar2.Value, x2 = trackBar4.Value, y2 = trackBar3.Value;

            imageFile = new Bitmap(fileName, true);

            for (int x = 0; x < imageFile.Width; x++)
            {
                for (int y = 0; y < imageFile.Height; y++)
                {
                    float r = imageFile.GetPixel(x, y).R, g = imageFile.GetPixel(x, y).G, b = imageFile.GetPixel(x, y).B;

                    if (r <= x1)
                    {
                        r = ((x1 - 0) == 0)? 0 : y1 - (((y1 - 0) * (x1 - (float)imageFile.GetPixel(x, y).R)) / (x1 - 0));
                    }
                    else if (x1 < r && r < x2)
                    {
                        r = ((x2 - x1) == 0)? 0 : y2 - (((y2 - y1) * (x2 - (float)imageFile.GetPixel(x, y).R)) / (x2 - x1));
                    }
                    else if (r >= x2)
                    {
                        r = ((255 - x2) == 0)? 0 : 255 - (((255 - y2) * (255 - (float)imageFile.GetPixel(x, y).R)) / (255 - x2));
                    }

                    if (g <= x1)
                    {
                        g = ((x1 - 0) == 0)? 0 : y1 - (((y1 - 0) * (x1 - (float)imageFile.GetPixel(x, y).G)) / (x1 - 0));
                    }
                    else if (x1 < g && g < x2)
                    {
                        g = ((x2 - x1) == 0)? 0 : y2 - (((y2 - y1) * (x2 - (float)imageFile.GetPixel(x, y).G)) / (x2 - x1));
                    }
                    else if (g >= x2)
                    {
                        g = ((255 - x2) == 0)? 0 : 255 - (((255 - y2) * (255 - (float)imageFile.GetPixel(x, y).G)) / (255 - x2));
                    }

                    if (b <= x1)
                    {
                        b = ((x1 - 0) == 0)? 0 : y1 - (((y1 - 0) * (x1 - (float)imageFile.GetPixel(x, y).B)) / (x1 - 0));
                    }
                    else if (x1 < b && b < x2)
                    {
                        b = ((x2 - x1) == 0)? 0 : y2 - (((y2 - y1) * (x2 - (float)imageFile.GetPixel(x, y).B)) / (x2 - x1));
                    }
                    else if (b >= x2)
                    {
                        b = ((255 - x2) == 0)? 0 : 255 - (((255 - y2) * (255 - (float)imageFile.GetPixel(x, y).B)) / (255 - x2));
                    }

                    Color newColor = Color.FromArgb((int)r, (int)g, (int)b);

                    imageFile.SetPixel(x, y, newColor);
                }
            }

            pictureBox3.Image = imageFile;
        }

        private void PixelCount()
        {
            for (int i = 0; i < 256; i++)
            {
                redCount[i] = 0;
                greenCount[i] = 0;
                blueCount[i] = 0;
            }

            for (int x = 0; x < imageFile.Width; x++)
            {
                for (int y = 0; y < imageFile.Height; y++)
                {
                    redCount[imageFile.GetPixel(x, y).R]++;
                    greenCount[imageFile.GetPixel(x, y).G]++;
                    blueCount[imageFile.GetPixel(x, y).B]++;
                }
            }
        }

        private float SigmaThis(int n, int mode)
        {
            float total = 0;
            for (int i = 0; i < n; i++)
            {
                if (mode == 0)
                {
                    total += redCount[i];
                }
                else if (mode == 1)
                {
                    total += greenCount[i];
                }
                else if (mode == 2)
                {
                    total += blueCount[i];
                }
            }
            return total;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            float size = imageFile.Width * imageFile.Height;
            PixelCount();
            for (int x = 0; x < imageFile.Width; x++)
            {
                for (int y = 0; y < imageFile.Height; y++)
                {
                    float r, g, b;

                    r = (SigmaThis(imageFile.GetPixel(x, y).R, 0) * 255.0f) / size;
                    g = (SigmaThis(imageFile.GetPixel(x, y).G, 1) * 255.0f) / size;
                    b = (SigmaThis(imageFile.GetPixel(x, y).B, 2) * 255.0f) / size;

                    Color newColor = Color.FromArgb((int)r, (int)g, (int)b);

                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            ContrastPoint();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            ContrastPoint();
        }

        private void trackBar4_Scroll(object sender, EventArgs e)
        {
            ContrastPoint();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            ContrastPoint();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (imageFile != null)
            {
                imageFile.Save(fileName.Substring(0, fileName.Length - 4) + "_modified_" + textBox7.Text + fileName.Substring(fileName.Length - 4, 4));
                MessageBox.Show(fileName.Substring(0, fileName.Length - 4) + "_modified_" + textBox7.Text + fileName.Substring(fileName.Length - 4, 4) + " has been successfully saved");
            }
            else
            {
                MessageBox.Show("No Image to be saved");
            }
        }

        void LaplaceFilter()
        {
            float maxRed = 0, minRed = 0, maxGreen = 0, minGreen = 0, maxBlue = 0, minBlue = 0;

            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 0f;
            filter[0, 1] = 1f;
            filter[0, 2] = 0f;
            filter[1, 0] = 1f;
            filter[1, 1] = -4f;
            filter[1, 2] = 1f;
            filter[2, 0] = 0f;
            filter[2, 1] = 1f;
            filter[2, 2] = 0f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green += defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue += defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }

                    if (x == aFilter && y == bFilter)
                    {
                        maxRed = red;
                        minRed = red;
                        maxGreen = green;
                        minGreen = green;
                        maxBlue = blue;
                        minBlue = blue;
                    }
                    else
                    {
                        maxRed = (red > maxRed) ? red : maxRed;
                        minRed = (red < minRed) ? red : minRed;
                        maxGreen = (green > maxGreen) ? green : maxGreen;
                        minGreen = (green < minGreen) ? green : minGreen;
                        maxBlue = (blue > maxBlue) ? blue : maxBlue;
                        minBlue = (blue < minBlue) ? blue : minBlue;
                    }

                    redFilter[x, y] = red;
                    greenFilter[x, y] = green;
                    blueFilter[x, y] = blue;
                }
            }

            for (int x = aFilter; x < imageFile.Width; x++)
            {
                for (int y = bFilter; y < imageFile.Height; y++)
                {
                    float red = redFilter[x, y], blue = blueFilter[x, y], green = greenFilter[x, y];
                    if (red < 0)
                    {
                        red = (minRed - red) * 127f / (minRed);
                    }
                    else if (red >= 0)
                    {
                        red = 127f + (red * 128f / (maxRed));
                    }

                    if (green < 0)
                    {
                        green = (minGreen - green) * 127f / (minGreen);
                    }
                    else if (green >= 0)
                    {
                        green = 127f + (green * 128f / (maxGreen));
                    }

                    if (blue < 0)
                    {
                        blue = (minBlue - blue) * 127f / (minBlue);
                    }
                    else if (blue >= 0)
                    {
                        blue = 127f + (blue * 128f / (maxBlue));
                    }

                    red = (float)defaultFile.GetPixel(x, y).R - red;
                    green = (float)defaultFile.GetPixel(x, y).G - green;
                    blue = (float)defaultFile.GetPixel(x, y).B - blue;

                    if (x == aFilter && y == bFilter)
                    {
                        maxRed = red;
                        minRed = red;
                        maxGreen = green;
                        minGreen = green;
                        maxBlue = blue;
                        minBlue = blue;
                    }
                    else
                    {
                        maxRed = (red > maxRed) ? red : maxRed;
                        minRed = (red < minRed) ? red : minRed;
                        maxGreen = (green > maxGreen) ? green : maxGreen;
                        minGreen = (green < minGreen) ? green : minGreen;
                        maxBlue = (blue > maxBlue) ? blue : maxBlue;
                        minBlue = (blue < minBlue) ? blue : minBlue;
                    }

                    redFilter[x, y] = red;
                    greenFilter[x, y] = green;
                    blueFilter[x, y] = blue;
                }
            }

            for (int x = aFilter; x < imageFile.Width; x++)
            {
                for (int y = bFilter; y < imageFile.Height; y++)
                {
                    float red = redFilter[x, y], blue = blueFilter[x, y], green = greenFilter[x, y];
                    if (red < 0)
                    {
                        red = (minRed - red) * 127f / (minRed);
                    }
                    else if (red >= 0)
                    {
                        red = 127f + (red * 128f / (maxRed));
                    }

                    if (green < 0)
                    {
                        green = (minGreen - green) * 127f / (minGreen);
                    }
                    else if (green >= 0)
                    {
                        green = 127f + (green * 128f / (maxGreen));
                    }

                    if (blue < 0)
                    {
                        blue = (minBlue - blue) * 127f / (minBlue);
                    }
                    else if (blue >= 0)
                    {
                        blue = 127f + (blue * 128f / (maxBlue));
                    }

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }

            pictureBox3.Image = imageFile;
        }

        void TrueLaplace()
        {
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 0f;
            filter[0, 1] = 1f;
            filter[0, 2] = 0f;
            filter[1, 0] = 1f;
            filter[1, 1] = -4f;
            filter[1, 2] = 1f;
            filter[2, 0] = 0f;
            filter[2, 1] = 1f;
            filter[2, 2] = 0f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green += defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue += defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }
                    red = (float)defaultFile.GetPixel(x, y).R - red;
                    green = (float)defaultFile.GetPixel(x, y).G - green;
                    blue = (float)defaultFile.GetPixel(x, y).B - blue;

                    red = (red < 0) ? 0 : red; green = (green < 0) ? 0 : green; blue = (blue < 0) ? 0 : blue;
                    red = (red > 255) ? 255 : red; green = (green > 255) ? 255 : green; blue = (blue > 255) ? 255 : blue;

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        void SobelFilter()
        {
            float maxRed = 0, minRed = 0, maxGreen = 0, minGreen = 0, maxBlue = 0, minBlue = 0;

            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = -1f;
            filter[0, 1] = -2f;
            filter[0, 2] = -1f;
            filter[1, 0] = 0;
            filter[1, 1] = 0;
            filter[1, 2] = 0;
            filter[2, 0] = 1;
            filter[2, 1] = 2;
            filter[2, 2] = 1;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green += defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue += defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }

                    if (x == aFilter && y == bFilter)
                    {
                        maxRed = red;
                        minRed = red;
                        maxGreen = green;
                        minGreen = green;
                        maxBlue = blue;
                        minBlue = blue;
                    }
                    else
                    {
                        maxRed = (red > maxRed) ? red : maxRed;
                        minRed = (red < minRed) ? red : minRed;
                        maxGreen = (green > maxGreen) ? green : maxGreen;
                        minGreen = (green < minGreen) ? green : minGreen;
                        maxBlue = (blue > maxBlue) ? blue : maxBlue;
                        minBlue = (blue < minBlue) ? blue : minBlue;
                    }

                    redFilter[x, y] = red;
                    greenFilter[x, y] = green;
                    blueFilter[x, y] = blue;
                }
            }

            for (int x = aFilter; x < imageFile.Width; x++)
            {
                for (int y = bFilter; y < imageFile.Height; y++)
                {
                    float red = redFilter[x, y], blue = blueFilter[x, y], green = greenFilter[x, y];
                    if (red < 0)
                    {
                        red = (minRed - red) * 127f / (minRed);
                    }
                    else if (red >= 0)
                    {
                        red = 127f + (red * 128f / (maxRed));
                    }

                    if (green < 0)
                    {
                        green = (minGreen - green) * 127f / (minGreen);
                    }
                    else if (green >= 0)
                    {
                        green = 127f + (green * 128f / (maxGreen));
                    }

                    if (blue < 0)
                    {
                        blue = (minBlue - blue) * 127f / (minBlue);
                    }
                    else if (blue >= 0)
                    {
                        blue = 127f + (blue * 128f / (maxBlue));
                    }

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        void TrueSobel()
        {
            Bitmap temporary = new Bitmap(fileName, true);

            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = -1f;
            filter[0, 1] = -2f;
            filter[0, 2] = -1f;
            filter[1, 0] = 0f;
            filter[1, 1] = 0f;
            filter[1, 2] = 0f;
            filter[2, 0] = 1f;
            filter[2, 1] = 2f;
            filter[2, 2] = 1f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green += defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue += defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }

                    red = (red < 0) ? 0 : red; green = (green < 0) ? 0 : green; blue = (blue < 0) ? 0 : blue;
                    red = (red > 255) ? 255 : red; green = (green > 255) ? 255 : green; blue = (blue > 255) ? 255 : blue;

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    temporary.SetPixel(x, y, newColor);
                }
            }

            filter[0, 0] = -1f;
            filter[0, 1] = 0f;
            filter[0, 2] = 1f;
            filter[1, 0] = -2f;
            filter[1, 1] = 0f;
            filter[1, 2] = 2f;
            filter[2, 0] = -1f;
            filter[2, 1] = 0f;
            filter[2, 2] = 1f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green += defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue += defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }

                    red = (red < 0) ? 0 : red; green = (green < 0) ? 0 : green; blue = (blue < 0) ? 0 : blue;
                    red = (red > 255) ? 255 : red; green = (green > 255) ? 255 : green; blue = (blue > 255) ? 255 : blue;

                    red = (red > temporary.GetPixel(x, y).R) ? red : temporary.GetPixel(x, y).R;
                    green = (green > temporary.GetPixel(x, y).G) ? green : temporary.GetPixel(x, y).G;
                    blue = (blue > temporary.GetPixel(x, y).B) ? blue : temporary.GetPixel(x, y).B;

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }

            pictureBox3.Image = imageFile;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            LaplaceFilter();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            SobelFilter();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            TrueLaplace();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            TrueSobel();
        }

        void SortAsc(float[] arr, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = i; j < size; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        float temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            //string test = "";
            //for (int i = 0; i < size; i++)
            //{
            //    test += arr[i] + " ";
            //}
            //MessageBox.Show(test);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            float[] redPixel = new float[m * n], greenPixel = new float[m * n], bluePixel = new float[m * n];

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    for (int i = -aFilter, count = 0; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++, count++)
                        {
                            redPixel[count] = (float)defaultFile.GetPixel(x + j, y + i).R;
                            greenPixel[count] = (float)defaultFile.GetPixel(x + j, y + i).G;
                            bluePixel[count] = (float)defaultFile.GetPixel(x + j, y + i).B;
                        }
                    }
                    SortAsc(redPixel, m * n);
                    SortAsc(greenPixel, m * n);
                    SortAsc(bluePixel, m * n);

                    Color newColor = Color.FromArgb((int)redPixel[4], (int)greenPixel[4], (int)bluePixel[4]);
                    imageFile.SetPixel(x, y, newColor);
                }
                pictureBox3.Image = imageFile;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 1f / (m * n);
            filter[0, 1] = 1f / (m * n);
            filter[0, 2] = 1f / (m * n);
            filter[1, 0] = 1f / (m * n);
            filter[1, 1] = 1f / (m * n);
            filter[1, 2] = 1f / (m * n);
            filter[2, 0] = 1f / (m * n);
            filter[2, 1] = 1f / (m * n);
            filter[2, 2] = 1f / (m * n);

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green += defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue += defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.InitialDirectory = Environment.CurrentDirectory;
                dlg.Title = "Open Image";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    fileName = dlg.FileName;
                    defaultFile = new Bitmap(fileName, true);
                    pictureBox2.Image = defaultFile;
                }
            }
        }

        void FourierTry()
        {
            int length = 2, width = 2;
            int[,] prototype = new int[length, width];
            prototype[0, 0] = 1;
            prototype[0, 1] = 2;
            prototype[1, 0] = 3;
            prototype[1, 1] = 4;

            ComplexNumber temp = new ComplexNumber();

            complex = new ComplexNumber[length, width];

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    complex[j, i] = new ComplexNumber();
                    complex[j, i].r = 0;
                    complex[j, i].i = 0;
                    for (int x = 0; x < length; x++)
                    {
                        for (int y = 0; y < width; y++)
                        {
                            float angle = (2f * 22f / 7f) * (((float)i * (float)x / (float)length) + ((float)j * (float)y / (float)width));
                            //MessageBox.Show(angle + "");
                            complex[j, i].r += prototype[y, x] * (float)Math.Cos(angle);
                            complex[j, i].i += (-1) * prototype[y, x] * (float)Math.Sin(angle);
                        }
                    }
                    complex[j, i].r /= ((float)length * (float)width);
                    complex[j, i].i /= ((float)length * (float)width);
                    //MessageBox.Show(complex[j, i].r + " " + complex[j, i].i);
                }
            }

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    temp.r = 0;
                    temp.i = 0;
                    for (int x = 0; x < length; x++)
                    {
                        for (int y = 0; y < width; y++)
                        {
                            float angle = (2f * 22f / 7f) * (((float)i * (float)x / (float)length) + ((float)j * (float)y / (float)width));
                            temp.r += complex[y, x].r * (float)Math.Cos(angle) - complex[y, x].i * (float)Math.Sin(angle);
                            MessageBox.Show(complex[y, x].r + " " + Math.Cos(angle) + " " + (-complex[y, x].i) + " " + Math.Sin(angle));
                        }
                        MessageBox.Show(temp.r + " ");
                    }
                    //MessageBox.Show(temp.r + " " + temp.i);
                }
            }
        }

        void FastFourierTry()
        {
            int length = 4, width = 2;
            int[] arr = new int[width * length];
            int[,] prototype = new int[width, length];
            prototype[0, 0] = 1;
            prototype[0, 1] = 2;
            prototype[0, 2] = 3;
            prototype[0, 3] = 4;
            prototype[1, 0] = 5;
            prototype[1, 1] = 6;
            prototype[1, 2] = 7;
            prototype[1, 3] = 8;

            for (int j = 0; j < width; j++)
            {
                for (int i = 0; i < length; i++)
                {
                    arr[j * length + i] = prototype[j, i];
                }
            }

            //for (int i = 0; i < width * length; i++)
            //{
            //    MessageBox.Show(" " + arr[i]);
            //}

            ComplexNumber temp = new ComplexNumber();

            complex = new ComplexNumber[width, length];

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    complex[j, i] = new ComplexNumber();
                    complex[j, i].r = 0;
                    complex[j, i].i = 0;
                    for (int x = 0; x < length; x++)
                    {
                        for (int y = 0; y < width; y++)
                        {
                            float angle = (2f * 22f / 7f) * (((float)i * (float)x / (float)length) + ((float)j * (float)y / (float)width));
                            //MessageBox.Show(angle + "");
                            complex[j, i].r += prototype[y, x] * (float)Math.Cos(angle);
                            complex[j, i].i += (-1) * prototype[y, x] * (float)Math.Sin(angle);
                        }
                    }
                    complex[j, i].r /= ((float)length * (float)width);
                    complex[j, i].i /= ((float)length * (float)width);
                    //MessageBox.Show(complex[j, i].r + " " + complex[j, i].i);
                }
            }

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    temp.r = 0;
                    temp.i = 0;
                    for (int x = 0; x < length; x++)
                    {
                        for (int y = 0; y < width; y++)
                        {
                            float angle = (2f * 22f / 7f) * (((float)i * (float)x / (float)length) + ((float)j * (float)y / (float)width));
                            temp.r += complex[y, x].r * (float)Math.Cos(angle) - complex[y, x].i * (float)Math.Sin(angle);
                        }
                    }
                    //MessageBox.Show(temp.r + " " + temp.i);
                }
            }

            ComplexNumber[] g = new ComplexNumber[(width * length) / 2];
            ComplexNumber[] h = new ComplexNumber[(width * length) / 2];
            ComplexNumber[] ans = new ComplexNumber[(width * length)];

            ComplexNumber[] complexArr = new ComplexNumber[width * length];

            for (int i = 0; i < (width * length) / 2; i++)
            {
                complexArr[i] = new ComplexNumber();
                complexArr[i].r = 0;
                complexArr[i].i = 0;
                float angle;
                for (int k = 0; k < width * length; k += 2)
                {
                    angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)width * (float)length);
                    complexArr[i].r += arr[k] * (float)Math.Cos(angle);
                    complexArr[i].i += (-1) * arr[k] * (float)Math.Sin(angle);
                }
                g[i] = new ComplexNumber();
                g[i].r = complexArr[i].r / ((float)length * (float)width);
                g[i].i = complexArr[i].i / ((float)length * (float)width);

                complexArr[i].r = 0;
                complexArr[i].i = 0;
                for (int k = 0; k < width * length; k += 2)
                {
                    angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)width * (float)length);
                    complexArr[i].r += arr[k + 1] * (float)Math.Cos(angle);
                    complexArr[i].i += (-1) * arr[k + 1] * (float)Math.Sin(angle);
                }
                h[i] = new ComplexNumber();
                h[i].r = complexArr[i].r / ((float)length * (float)width);
                h[i].i = complexArr[i].i / ((float)length * (float)width);

                angle = ((2f * 22f / 7f) * (float)i) / ((float)width * (float)length);
                ans[i] = new ComplexNumber();
                ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);
            }

            for (int i = (width * length) / 2, j = 0; i < width * length; i++, j++)
            {
                float angle = ((2f * 22f / 7f) * (float)i) / ((float)width * (float)length);
                ans[i] = new ComplexNumber();
                ans[i].r = g[j].r + ((float)Math.Cos(angle) * h[j].r) + ((float)Math.Sin(angle) * h[j].i);
                ans[i].i = g[j].i + ((float)Math.Cos(angle) * h[j].i) + ((-1) * (float)Math.Sin(angle) * h[j].r);
            }

            for (int i = 0; i < width * length; i++)
            {
                MessageBox.Show(ans[i].r + " " + ans[i].i);
            }
        }

        void FastFourierTry2D()
        {
            int length = 6, width = 4, row, col;
            int[,] prototype = new int[width, length];
            int number = 1;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    prototype[i, j] = number;
                    number += 1;
                }
            }

            row = (width / 2);
            col = (length / 2);

            ComplexNumber[,] compArr = new ComplexNumber[width, length];
            ComplexNumber[,] protoman = new ComplexNumber[length, width];

            //Fourier Column
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, k] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, k] * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, (k + 1)] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, (k + 1)] * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            int swap = length;
            length = width;
            width = swap;

            //Fourier Row
            compArr = new ComplexNumber[width, length];
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Col
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Row
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r * length * width;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    MessageBox.Show(protoman[i, j].r + " " + protoman[i, j].i);
                }
            }
        }

        void FailedFourier2D()
        {
            //ComplexNumber[,] g = new ComplexNumber[(width / 2), (length / 2)];
            //ComplexNumber[,] h = new ComplexNumber[(width / 2), (length / 2)];
            //ComplexNumber[,] ans = new ComplexNumber[width, length];
            //ComplexNumber temp = new ComplexNumber();

            //ComplexNumber[] gt = new ComplexNumber[(length / 2)];
            //ComplexNumber[] ht = new ComplexNumber[(length / 2)];
            //ComplexNumber[] anst = new ComplexNumber[(length / 2)];

            //row = (width / 2);
            //col = (length / 2);
            //for (int y = 0; y < row; y++)
            //{
            //    for (int x = 0; x < col; x++)
            //    {
            //        g[y, x] = new ComplexNumber();
            //        h[y, x] = new ComplexNumber();
            //        ans[y, x] = new ComplexNumber();
            //        for (int i = 0; i < width; i += 2)
            //        {
            //            gt[(i / 2)] = new ComplexNumber();
            //            ht[(i / 2)] = new ComplexNumber();
            //            anst[(i / 2)] = new ComplexNumber();
            //            float angle;

            //            temp.r = 0;
            //            temp.i = 0;
            //            for (int j = 0; j < length; j += 2)
            //            {
            //                angle = (2f * 22f / 7f) * (((float)x * (float)j / (float)length) + ((float)y * (float)i / (float)width));
            //                //MessageBox.Show(angle + "");
            //                temp.r += prototype[i, j] * (float)Math.Cos(angle);
            //                temp.i += (-1) * prototype[i, j] * (float)Math.Sin(angle);
            //            }
            //            gt[(i / 2)].r = (temp.r / ((float)length * (float)width));
            //            gt[(i / 2)].i = (temp.i / ((float)length * (float)width));

            //            temp.r = 0;
            //            temp.i = 0;
            //            for (int j = 0; j < length; j += 2)
            //            {
            //                angle = (2f * 22f / 7f) * (((float)x * (float)(j + 1) / (float)length) + ((float)y * (float)i / (float)width));
            //                //MessageBox.Show(angle + "");
            //                temp.r += prototype[i, (j + 1)] * (float)Math.Cos(angle);
            //                temp.i += (-1) * prototype[i, (j + 1)] * (float)Math.Sin(angle);
            //            }
            //            ht[(i / 2)].r = (temp.r / ((float)length * (float)width));
            //            ht[(i / 2)].i = (temp.i / ((float)length * (float)width));

            //            angle = (2f * 22f / 7f) * (((float)x / (float)length) + ((float)y / (float)width));
            //            anst[(i / 2)].r = gt[(i / 2)].r + ((float)Math.Cos(angle) * ht[(i / 2)].r) + ((float)Math.Sin(angle) * ht[(i / 2)].i);
            //            anst[(i / 2)].i = gt[(i / 2)].i + ((float)Math.Cos(angle) * ht[(i / 2)].i) + ((-1) * (float)Math.Sin(angle) * ht[(i / 2)].r);
            //        }
            //    }
            //}

            //ComplexNumber[] complexArr = new ComplexNumber[width * length];

            //for (int i = 0; i < (width * length) / 2; i++)
            //{
            //    complexArr[i] = new ComplexNumber();
            //    complexArr[i].r = 0;
            //    complexArr[i].i = 0;
            //    float angle;
            //    for (int k = 0; k < width * length; k += 2)
            //    {
            //        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)width * (float)length);
            //        complexArr[i].r += arr[k] * (float)Math.Cos(angle);
            //        complexArr[i].i += (-1) * arr[k] * (float)Math.Sin(angle);
            //    }
            //    g[i] = new ComplexNumber();
            //    g[i].r = complexArr[i].r / ((float)length * (float)width);
            //    g[i].i = complexArr[i].i / ((float)length * (float)width);

            //    complexArr[i].r = 0;
            //    complexArr[i].i = 0;
            //    for (int k = 0; k < width * length; k += 2)
            //    {
            //        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)width * (float)length);
            //        complexArr[i].r += arr[k + 1] * (float)Math.Cos(angle);
            //        complexArr[i].i += (-1) * arr[k + 1] * (float)Math.Sin(angle);
            //    }
            //    h[i] = new ComplexNumber();
            //    h[i].r = complexArr[i].r / ((float)length * (float)width);
            //    h[i].i = complexArr[i].i / ((float)length * (float)width);

            //    angle = ((2f * 22f / 7f) * (float)i) / ((float)width * (float)length);
            //    ans[i] = new ComplexNumber();
            //    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
            //    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);
            //}

            //for (int i = (width * length) / 2, j = 0; i < width * length; i++, j++)
            //{
            //    float angle = ((2f * 22f / 7f) * (float)i) / ((float)width * (float)length);
            //    ans[i] = new ComplexNumber();
            //    ans[i].r = g[j].r + ((float)Math.Cos(angle) * h[j].r) + ((float)Math.Sin(angle) * h[j].i);
            //    ans[i].i = g[j].i + ((float)Math.Cos(angle) * h[j].i) + ((-1) * (float)Math.Sin(angle) * h[j].r);
            //}

            //for (int i = 0; i < width * length; i++)
            //{
            //    MessageBox.Show(ans[i].r + " " + ans[i].i);
            //}
        }

        void FailedWhatever()
        {
            //FourierTry();
            imageFile = new Bitmap(fileName, true);
            int length = imageFile.Width, width = imageFile.Height;
            int d0 = int.Parse(textBox5.Text);
            int[,] prototype = new int[width, length];
            int[,] prototypeR = new int[width, length], prototypeG = new int[width, length], prototypeB = new int[width, length];

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    prototypeR[j, i] = (int)(imageFile.GetPixel(i, j).R * Math.Pow(-1, i + j));
                    prototypeG[j, i] = (int)(imageFile.GetPixel(i, j).G * Math.Pow(-1, i + j));
                    prototypeB[j, i] = (int)(imageFile.GetPixel(i, j).B * Math.Pow(-1, i + j));
                }
            }

            ComplexNumber temp = new ComplexNumber();
            ComplexNumber tempR = new ComplexNumber(), tempG = new ComplexNumber(), tempB = new ComplexNumber();

            complex = new ComplexNumber[width, length];
            complexR = new ComplexNumber[width, length];
            complexG = new ComplexNumber[width, length];
            complexB = new ComplexNumber[width, length];

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    complexR[j, i] = new ComplexNumber();
                    complexG[j, i] = new ComplexNumber();
                    complexB[j, i] = new ComplexNumber();

                    complexR[j, i].r = 0;
                    complexR[j, i].i = 0;

                    complexG[j, i].r = 0;
                    complexG[j, i].i = 0;

                    complexB[j, i].r = 0;
                    complexB[j, i].i = 0;

                    for (int x = 0; x < length; x++)
                    {
                        for (int y = 0; y < width; y++)
                        {
                            float angle = (2f * 22f / 7f) * (((float)i * (float)x / (float)length) + ((float)j * (float)y / (float)width));
                            //MessageBox.Show(angle + "");
                            complexR[j, i].r += prototypeR[y, x] * (float)Math.Cos(angle);
                            complexR[j, i].i += (-1) * prototypeR[y, x] * (float)Math.Sin(angle);

                            complexG[j, i].r += prototypeG[y, x] * (float)Math.Cos(angle);
                            complexG[j, i].i += (-1) * prototypeG[y, x] * (float)Math.Sin(angle);

                            complexB[j, i].r += prototypeB[y, x] * (float)Math.Cos(angle);
                            complexB[j, i].i += (-1) * prototypeB[y, x] * (float)Math.Sin(angle);
                        }
                    }
                    complexR[j, i].r /= ((float)length * (float)width);
                    complexR[j, i].i /= ((float)length * (float)width);

                    complexG[j, i].r /= ((float)length * (float)width);
                    complexG[j, i].i /= ((float)length * (float)width);

                    complexB[j, i].r /= ((float)length * (float)width);
                    complexB[j, i].i /= ((float)length * (float)width);

                    float distance = (float)Math.Sqrt(Math.Pow((i - length / 2), 2) + Math.Pow((j - width / 2), 2));

                    complexR[j, i].r = (distance <= d0) ? complexR[j, i].r : 0;
                    complexR[j, i].i = (distance <= d0) ? complexR[j, i].i : 0;

                    complexG[j, i].r = (distance <= d0) ? complexG[j, i].r : 0;
                    complexG[j, i].i = (distance <= d0) ? complexG[j, i].i : 0;

                    complexB[j, i].r = (distance <= d0) ? complexB[j, i].r : 0;
                    complexB[j, i].i = (distance <= d0) ? complexB[j, i].i : 0;

                    //MessageBox.Show(complex[j, i].r + " " + complex[j, i].i);
                }
            }

            //for (int i = 0; i < length; i++)
            //{
            //    for (int j = 0; j < width; j++)
            //    {
            //        int value = (int)Math.Sqrt(Math.Pow(complex[j, i].r, 2) + Math.Pow(complex[j, i].i, 2));
            //        Color newColor = Color.FromArgb(value, value, value);
            //        imageFile.SetPixel(i, j, newColor);
            //    }
            //}

            //pictureBox3.Image = imageFile;

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    temp.r = 0;
                    temp.i = 0;

                    tempR.r = 0;
                    tempR.i = 0;

                    tempG.r = 0;
                    tempG.i = 0;

                    tempB.r = 0;
                    tempB.i = 0;
                    for (int x = 0; x < length; x++)
                    {
                        for (int y = 0; y < width; y++)
                        {
                            float angle = (2f * 22f / 7f) * (((float)i * (float)x / (float)length) + ((float)j * (float)y / (float)width));
                            //temp.r += complex[y, x].r * (float)Math.Cos(angle) - complex[y, x].i * (float)Math.Sin(angle);

                            tempR.r += complexR[y, x].r * (float)Math.Cos(angle) - complexR[y, x].i * (float)Math.Sin(angle);
                            tempG.r += complexG[y, x].r * (float)Math.Cos(angle) - complexG[y, x].i * (float)Math.Sin(angle);
                            tempB.r += complexB[y, x].r * (float)Math.Cos(angle) - complexB[y, x].i * (float)Math.Sin(angle);
                        }
                    }
                    //prototype[j, i] = (int)temp.r;

                    prototypeR[j, i] = (int)tempR.r;
                    prototypeG[j, i] = (int)tempG.r;
                    prototypeB[j, i] = (int)tempB.r;
                }
            }

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    prototypeR[j, i] = (int)(prototypeR[j, i] * Math.Pow(-1, i + j));
                    prototypeG[j, i] = (int)(prototypeG[j, i] * Math.Pow(-1, i + j));
                    prototypeB[j, i] = (int)(prototypeB[j, i] * Math.Pow(-1, i + j));

                    prototypeR[j, i] = (prototypeR[j, i] > 255) ? 255 : prototypeR[j, i];
                    prototypeG[j, i] = (prototypeG[j, i] > 255) ? 255 : prototypeG[j, i];
                    prototypeB[j, i] = (prototypeB[j, i] > 255) ? 255 : prototypeB[j, i];

                    prototypeR[j, i] = (prototypeR[j, i] < 0) ? 0 : prototypeR[j, i];
                    prototypeG[j, i] = (prototypeG[j, i] < 0) ? 0 : prototypeG[j, i];
                    prototypeB[j, i] = (prototypeB[j, i] < 0) ? 0 : prototypeB[j, i];
                }
            }

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Color newColor = Color.FromArgb(prototypeR[j, i], prototypeG[j, i], prototypeB[j, i]);
                    imageFile.SetPixel(i, j, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            PictureBox[] picArr = new PictureBox[3];
            picArr[0] = pictureBox3;
            picArr[1] = pictureBox1;
            picArr[2] = pictureBox4;
            float r, g, b;
            float h0l = 0.5f, h0r = 0.5f, h1l = 0.5f, h1r = -0.5f;
            int k_value = 3;
            List<List<float>> rArr = new List<List<float>>(), gArr = new List<List<float>>(), bArr = new List<List<float>>();

            imageFile = new Bitmap(fileName, true);
            Bitmap[] images = new Bitmap[k_value];
            for (int k = 0; k < k_value; k++)
            {
                images[k] = new Bitmap(imageFile);
                rArr.Clear();
                gArr.Clear();
                bArr.Clear();
                for (int i = 0; i < imageFile.Height / (Math.Pow(2.0, k)); i++)
                {
                    rArr.Add(new List<float>());
                    gArr.Add(new List<float>());
                    bArr.Add(new List<float>());
                    for (int j = 0; j < imageFile.Width / (Math.Pow(2.0, k)); j += 2)
                    {
                        r = (imageFile.GetPixel(j, i).R * h0l) + (imageFile.GetPixel(j + 1, i).R * h0r);
                        g = (imageFile.GetPixel(j, i).G * h0l) + (imageFile.GetPixel(j + 1, i).G * h0r);
                        b = (imageFile.GetPixel(j, i).B * h0l) + (imageFile.GetPixel(j + 1, i).B * h0r);

                        rArr[i].Add(r);
                        gArr[i].Add(g);
                        bArr[i].Add(b);
                    }
                    for (int j = 0; j < imageFile.Width / (Math.Pow(2.0, k)); j += 2)
                    {
                        r = (imageFile.GetPixel(j, i).R * h1l) + (imageFile.GetPixel(j + 1, i).R * h1r);
                        g = (imageFile.GetPixel(j, i).G * h1l) + (imageFile.GetPixel(j + 1, i).G * h1r);
                        b = (imageFile.GetPixel(j, i).B * h1l) + (imageFile.GetPixel(j + 1, i).B * h1r);

                        rArr[i].Add(r);
                        gArr[i].Add(g);
                        bArr[i].Add(b);
                    }
                }

                float[,] RArr = new float[rArr.Count, rArr[0].Count], GArr = new float[gArr.Count, gArr[0].Count], BArr = new float[bArr.Count, bArr[0].Count];

                for (int i = 0; i < rArr[0].Count; i++)
                {
                    int count = 0;
                    for (int j = 0; j < rArr.Count; j += 2)
                    {
                        if (j + 1 < rArr.Count)
                        {
                            r = (rArr[j][i] * h0l) + (rArr[j + 1][i] * h0r);
                            g = (gArr[j][i] * h0l) + (gArr[j + 1][i] * h0r);
                            b = (bArr[j][i] * h0l) + (bArr[j + 1][i] * h0r);

                            RArr[count, i] = r;
                            GArr[count, i] = g;
                            BArr[count, i] = b;
                            count++;
                        }
                    }
                    for (int j = 0; j < rArr.Count; j += 2)
                    {
                        if (j + 1 < rArr.Count)
                        {
                            r = (rArr[j][i] * h1l) + (rArr[j + 1][i] * h1r);
                            g = (gArr[j][i] * h1l) + (gArr[j + 1][i] * h1r);
                            b = (bArr[j][i] * h1l) + (bArr[j + 1][i] * h1r);

                            RArr[count, i] = r;
                            GArr[count, i] = g;
                            BArr[count, i] = b;
                            count++;
                        }
                    }
                }

                for (int i = 0; i < rArr.Count; i++)
                {
                    for (int j = 0; j < rArr[i].Count; j++)
                    {
                        RArr[i, j] = (RArr[i, j] < 0) ? 0 : RArr[i, j];
                        GArr[i, j] = (GArr[i, j] < 0) ? 0 : GArr[i, j];
                        BArr[i, j] = (BArr[i, j] < 0) ? 0 : BArr[i, j];

                        imageFile.SetPixel(j, i, Color.FromArgb((int)RArr[i, j], (int)GArr[i, j], (int)BArr[i, j]));
                        images[k].SetPixel(j, i, Color.FromArgb((int)RArr[i, j], (int)GArr[i, j], (int)BArr[i, j]));
                    }
                }
                picArr[k].Image = images[k];
            }
        }

        class ComplexNumber
        {
            public float r, i;
        };

        private void button19_Click(object sender, EventArgs e)
        {
            FourierTry();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.Equals("Ideal"))
            {
                IdealHighPassFFT();
            }
            else if (comboBox1.SelectedItem.Equals("Gaussian"))
            {
                GaussianHighPassFFT();
            }
        }

        void IdealLowPassFFT()
        {
            Bitmap tempFile = new Bitmap(fileName, true);
            
            int length = tempFile.Width, width = tempFile.Height;
            
            length = (length % 2 == 0) ? length : (length + 1);
            width = (width % 2 == 0) ? width : (width + 1);
            
            imageFile = new Bitmap(length, width);

            Bitmap fdFile = new Bitmap(length, width);
            Bitmap fdfFile = new Bitmap(length, width);

            for (int i = 0; i < tempFile.Height; i++)
            {
                for (int j = 0; j < tempFile.Width; j++)
                {
                    imageFile.SetPixel(j, i, tempFile.GetPixel(j, i));
                }
            }
            
            int d0 = int.Parse(textBox5.Text);
            float total;
            int row, col;
            int[,] prototype = new int[width, length];
            float[,] ratioR = new float[width, length], ratioG = new float[width, length], ratioB = new float[width, length];
            float[,] fd = new float[width, length];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    total = (imageFile.GetPixel(j, i).R + imageFile.GetPixel(j, i).G + imageFile.GetPixel(j, i).B);
                    ratioR[i, j] = imageFile.GetPixel(j, i).R / total;
                    ratioG[i, j] = imageFile.GetPixel(j, i).G / total;
                    ratioB[i, j] = imageFile.GetPixel(j, i).B / total;
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(total / 3);
                }
            }

            row = (width / 2);
            col = (length / 2);

            ComplexNumber[,] compArr = new ComplexNumber[width, length];
            ComplexNumber[,] protoman = new ComplexNumber[length, width];

            //Fourier Column
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, k] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, k] * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, (k + 1)] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, (k + 1)] * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            int swap = length;
            length = width;
            width = swap;

            //Fourier Row
            compArr = new ComplexNumber[width, length];
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            float max = -1;
            float min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(0.2f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox1.Image = fdFile;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float distance = (float)Math.Sqrt(Math.Pow((i - (length / 2)), 2) + Math.Pow((j - width / 2), 2));
                    compArr[i, j].r = (distance <= d0) ? compArr[i, j].r : 0;
                    compArr[i, j].i = (distance <= d0) ? compArr[i, j].i : 0;
                }
            }

            max = -1;
            min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdfFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox4.Image = fdfFile;

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Col
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Row
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r * length * width;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            float red, green, blue;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(protoman[i, j].r);
                    prototype[i, j] = (prototype[i, j] < 0) ? 0 : prototype[i, j];
                    prototype[i, j] = (prototype[i, j] > 255) ? 255 : prototype[i, j];

                    red = prototype[i, j] * 3 * ratioR[i, j];
                    red = (red > 255) ? 255 : red;

                    green = prototype[i, j] * 3 * ratioG[i, j];
                    green = (green > 255) ? 255 : green;

                    blue = prototype[i, j] * 3 * ratioB[i, j];
                    blue = (blue > 255) ? 255 : blue;

                    try
                    {
                        Color color = Color.FromArgb((int)red, (int)green, (int)blue);
                        //Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                    catch
                    {
                        Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        //MessageBox.Show(i + " " + j);
                        //MessageBox.Show(ratioR[i, j] + " " + ratioG[i, j] + " " + ratioB[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                }
            }
            //complexR[j, i].r = (distance <= d0) ? complexR[j, i].r : 0;
            //complexR[j, i].i = (distance <= d0) ? complexR[j, i].i : 0;

            pictureBox3.Image = imageFile;
        }

        void ButterworthLowPassFFT()
        {
            Bitmap tempFile = new Bitmap(fileName, true);

            int length = tempFile.Width, width = tempFile.Height;

            length = (length % 2 == 0) ? length : (length + 1);
            width = (width % 2 == 0) ? width : (width + 1);

            imageFile = new Bitmap(length, width);

            Bitmap fdFile = new Bitmap(length, width);
            Bitmap fdfFile = new Bitmap(length, width);

            for (int i = 0; i < tempFile.Height; i++)
            {
                for (int j = 0; j < tempFile.Width; j++)
                {
                    imageFile.SetPixel(j, i, tempFile.GetPixel(j, i));
                }
            }

            int d0 = int.Parse(textBox5.Text);
            int n = int.Parse(textBox6.Text);
            float total;
            int row, col;
            int[,] prototype = new int[width, length];
            float[,] ratioR = new float[width, length], ratioG = new float[width, length], ratioB = new float[width, length];
            float[,] fd = new float[width, length];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    total = (imageFile.GetPixel(j, i).R + imageFile.GetPixel(j, i).G + imageFile.GetPixel(j, i).B);
                    ratioR[i, j] = imageFile.GetPixel(j, i).R / total;
                    ratioG[i, j] = imageFile.GetPixel(j, i).G / total;
                    ratioB[i, j] = imageFile.GetPixel(j, i).B / total;
                    //MessageBox.Show(total + " " + ratioR[i, j] + " " + ratioG[i, j] + " " + ratioB[i, j]);
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(total / 3);
                }
            }

            row = (width / 2);
            col = (length / 2);

            ComplexNumber[,] compArr = new ComplexNumber[width, length];
            ComplexNumber[,] protoman = new ComplexNumber[length, width];

            //Fourier Column
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, k] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, k] * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, (k + 1)] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, (k + 1)] * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            int swap = length;
            length = width;
            width = swap;

            //Fourier Row
            compArr = new ComplexNumber[width, length];
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            float max = -1;
            float min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox1.Image = fdFile;


            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float distance = (float)Math.Sqrt(Math.Pow((i - length / 2), 2) + Math.Pow((j - width / 2), 2));
                    float inter = (distance / (float)d0);
                    float bw = 1.0f + (float)Math.Pow(inter, 2 * n);
                    //MessageBox.Show(bw + " ");
                    compArr[i, j].r *= (1.0f / bw);
                    compArr[i, j].i *= (1.0f / bw);
                }
            }

            max = -1;
            min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdfFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox4.Image = fdfFile;

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Col
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Row
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r * length * width;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            float red, green, blue;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(protoman[i, j].r);
                    prototype[i, j] = (prototype[i, j] < 0) ? 0 : prototype[i, j];
                    prototype[i, j] = (prototype[i, j] > 255) ? 255 : prototype[i, j];

                    red = prototype[i, j] * 3 * ratioR[i, j];
                    red = (red > 255) ? 255 : red;

                    green = prototype[i, j] * 3 * ratioG[i, j];
                    green = (green > 255) ? 255 : green;

                    blue = prototype[i, j] * 3 * ratioB[i, j];
                    blue = (blue > 255) ? 255 : blue;

                    try
                    {
                        Color color = Color.FromArgb((int)red, (int)green, (int)blue);
                        //Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                    catch
                    {
                        Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        //MessageBox.Show(i + " " + j);
                        //MessageBox.Show(ratioR[i, j] + " " + ratioG[i, j] + " " + ratioB[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                }
            }
            //complexR[j, i].r = (distance <= d0) ? complexR[j, i].r : 0;
            //complexR[j, i].i = (distance <= d0) ? complexR[j, i].i : 0;

            pictureBox3.Image = imageFile;
        }

        void IdealHighPassFFT()
        {
            Bitmap tempFile = new Bitmap(fileName, true);

            int length = tempFile.Width, width = tempFile.Height;

            length = (length % 2 == 0) ? length : (length + 1);
            width = (width % 2 == 0) ? width : (width + 1);

            imageFile = new Bitmap(length, width);

            Bitmap fdFile = new Bitmap(length, width);
            Bitmap fdfFile = new Bitmap(length, width);

            for (int i = 0; i < tempFile.Height; i++)
            {
                for (int j = 0; j < tempFile.Width; j++)
                {
                    imageFile.SetPixel(j, i, tempFile.GetPixel(j, i));
                }
            }

            int d0 = int.Parse(textBox5.Text);
            float total;
            int row, col;
            int[,] prototype = new int[width, length];
            float[,] ratioR = new float[width, length], ratioG = new float[width, length], ratioB = new float[width, length];

            float[,] fd = new float[width, length];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    total = (imageFile.GetPixel(j, i).R + imageFile.GetPixel(j, i).G + imageFile.GetPixel(j, i).B);
                    ratioR[i, j] = imageFile.GetPixel(j, i).R / total;
                    ratioG[i, j] = imageFile.GetPixel(j, i).G / total;
                    ratioB[i, j] = imageFile.GetPixel(j, i).B / total;
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(total / 3);
                }
            }

            row = (width / 2);
            col = (length / 2);

            ComplexNumber[,] compArr = new ComplexNumber[width, length];
            ComplexNumber[,] protoman = new ComplexNumber[length, width];

            //Fourier Column
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, k] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, k] * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, (k + 1)] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, (k + 1)] * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            int swap = length;
            length = width;
            width = swap;

            //Fourier Row
            compArr = new ComplexNumber[width, length];
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            float max = -1;
            float min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox1.Image = fdFile;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float distance = (float)Math.Sqrt(Math.Pow((i - length / 2), 2) + Math.Pow((j - width / 2), 2));
                    compArr[i, j].r = (distance >= d0) ? compArr[i, j].r : 0;
                    compArr[i, j].i = (distance >= d0) ? compArr[i, j].i : 0;
                }
            }

            max = -1;
            min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdfFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox4.Image = fdfFile;

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Col
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Row
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r * length * width;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            float red, green, blue;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(protoman[i, j].r);
                    prototype[i, j] = (prototype[i, j] < 0) ? 0 : prototype[i, j];
                    prototype[i, j] = (prototype[i, j] > 255) ? 255 : prototype[i, j];

                    red = prototype[i, j] * 3 * ratioR[i, j];
                    red = (red > 255) ? 255 : red;

                    green = prototype[i, j] * 3 * ratioG[i, j];
                    green = (green > 255) ? 255 : green;

                    blue = prototype[i, j] * 3 * ratioB[i, j];
                    blue = (blue > 255) ? 255 : blue;

                    try
                    {
                        Color color = Color.FromArgb((int)red, (int)green, (int)blue);
                        //Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                    catch
                    {
                        Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        //MessageBox.Show(i + " " + j);
                        //MessageBox.Show(ratioR[i, j] + " " + ratioG[i, j] + " " + ratioB[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                }
            }
            //complexR[j, i].r = (distance <= d0) ? complexR[j, i].r : 0;
            //complexR[j, i].i = (distance <= d0) ? complexR[j, i].i : 0;

            pictureBox3.Image = imageFile;
        }

        void GaussianHighPassFFT()
        {
            Bitmap tempFile = new Bitmap(fileName, true);

            int length = tempFile.Width, width = tempFile.Height;

            length = (length % 2 == 0) ? length : (length + 1);
            width = (width % 2 == 0) ? width : (width + 1);

            imageFile = new Bitmap(length, width);

            Bitmap fdFile = new Bitmap(length, width);
            Bitmap fdfFile = new Bitmap(length, width);

            for (int i = 0; i < tempFile.Height; i++)
            {
                for (int j = 0; j < tempFile.Width; j++)
                {
                    imageFile.SetPixel(j, i, tempFile.GetPixel(j, i));
                }
            }

            int d0 = int.Parse(textBox5.Text);
            float total;
            int row, col;
            int[,] prototype = new int[width, length];
            float[,] ratioR = new float[width, length], ratioG = new float[width, length], ratioB = new float[width, length];
            float[,] fd = new float[width, length];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    total = (imageFile.GetPixel(j, i).R + imageFile.GetPixel(j, i).G + imageFile.GetPixel(j, i).B);
                    ratioR[i, j] = imageFile.GetPixel(j, i).R / total;
                    ratioG[i, j] = imageFile.GetPixel(j, i).G / total;
                    ratioB[i, j] = imageFile.GetPixel(j, i).B / total;
                    //MessageBox.Show(total + " " + ratioR[i, j] + " " + ratioG[i, j] + " " + ratioB[i, j]);
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(total / 3);
                }
            }

            row = (width / 2);
            col = (length / 2);

            ComplexNumber[,] compArr = new ComplexNumber[width, length];
            ComplexNumber[,] protoman = new ComplexNumber[length, width];

            //Fourier Column
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, k] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, k] * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += prototype[j, (k + 1)] * (float)Math.Cos(angle);
                        complexArr[i].i += (-1) * prototype[j, (k + 1)] * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            int swap = length;
            length = width;
            width = swap;

            //Fourier Row
            compArr = new ComplexNumber[width, length];
            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            float max = -1;
            float min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox1.Image = fdFile;

            float e = 2.71828182846f;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float distance = (float)Math.Sqrt(Math.Pow((i - length / 2), 2) + Math.Pow((j - width / 2), 2));
                    float inter = (-1.0f) * (float)(Math.Pow(distance, 2.0) / (2.0 * Math.Pow((float)d0, 2.0)));
                    float gauss = (float)(1 - Math.Pow(e, inter));
                    //MessageBox.Show(bw + " ");
                    compArr[i, j].r *= gauss;
                    compArr[i, j].i *= gauss;
                }
            }

            max = -1;
            min = -1;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    float magnitude = (float)Math.Sqrt(Math.Pow(compArr[i, j].r, 2.0f) + Math.Pow(compArr[i, j].i, 2.0f));
                    fd[j, i] = (float)Math.Log(1.0f + magnitude);
                    if (max == -1)
                    {
                        max = fd[j, i];
                    }
                    else
                    {
                        max = (fd[j, i] > max) ? fd[j, i] : max;
                    }

                    if (min == -1)
                    {
                        min = fd[j, i];
                    }
                    else
                    {
                        min = (fd[j, i] < min) ? fd[j, i] : min;
                    }
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    fd[j, i] = ((fd[j, i] - min) / (max - min)) * 255;
                    fdfFile.SetPixel(i, j, Color.FromArgb((int)fd[j, i], (int)fd[j, i], (int)fd[j, i]));
                }
            }

            pictureBox4.Image = fdfFile;

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Col
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r;
                    protoman[j, i].i = compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            //Inverse Row
            compArr = new ComplexNumber[width, length];

            for (int j = 0; j < width; j++)
            {
                ComplexNumber[] g = new ComplexNumber[(length / 2)];
                ComplexNumber[] h = new ComplexNumber[(length / 2)];
                ComplexNumber[] ans = new ComplexNumber[length];

                ComplexNumber[] complexArr = new ComplexNumber[length];

                for (int i = 0; i < (length / 2); i++)
                {
                    complexArr[i] = new ComplexNumber();
                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    float angle;
                    for (int k = 0; k < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, k].r * (float)Math.Cos(angle) + protoman[j, k].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, k].i * (float)Math.Cos(angle) + (-1) * protoman[j, k].r * (float)Math.Sin(angle);
                    }
                    g[i] = new ComplexNumber();
                    g[i].r = complexArr[i].r / ((float)length);
                    g[i].i = complexArr[i].i / ((float)length);

                    complexArr[i].r = 0;
                    complexArr[i].i = 0;
                    for (int k = 0; (k + 1) < length; k += 2)
                    {
                        angle = ((2f * 22f / 7f) * (float)i * (float)k) / ((float)length);
                        complexArr[i].r += protoman[j, (k + 1)].r * (float)Math.Cos(angle) + protoman[j, (k + 1)].i * (float)Math.Sin(angle);
                        complexArr[i].i += protoman[j, (k + 1)].i * (float)Math.Cos(angle) + (-1) * protoman[j, (k + 1)].r * (float)Math.Sin(angle);
                    }
                    h[i] = new ComplexNumber();
                    h[i].r = complexArr[i].r / ((float)length);
                    h[i].i = complexArr[i].i / ((float)length);

                    angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[i].r + ((float)Math.Cos(angle) * h[i].r) + ((float)Math.Sin(angle) * h[i].i);
                    ans[i].i = g[i].i + ((float)Math.Cos(angle) * h[i].i) + ((-1) * (float)Math.Sin(angle) * h[i].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;
                }

                for (int i = (length / 2), k = 0; i < length; i++)
                {
                    float angle = ((2f * 22f / 7f) * (float)i) / ((float)length);
                    ans[i] = new ComplexNumber();
                    ans[i].r = g[k].r + ((float)Math.Cos(angle) * h[k].r) + ((float)Math.Sin(angle) * h[k].i);
                    ans[i].i = g[k].i + ((float)Math.Cos(angle) * h[k].i) + ((-1) * (float)Math.Sin(angle) * h[k].r);

                    compArr[j, i] = new ComplexNumber();
                    compArr[j, i].r = ans[i].r;
                    compArr[j, i].i = ans[i].i;

                    if ((k + 1) < (length / 2))
                    {
                        k++;
                    }
                }
            }

            protoman = new ComplexNumber[length, width];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    protoman[j, i] = new ComplexNumber();
                    protoman[j, i].r = compArr[i, j].r * length * width;
                    protoman[j, i].i = (-1) * compArr[i, j].i;
                }
            }

            swap = length;
            length = width;
            width = swap;

            float red, green, blue;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    prototype[i, j] = (int)Math.Pow(-1, i + j) * (int)(protoman[i, j].r);
                    prototype[i, j] = (prototype[i, j] < 0) ? 0 : prototype[i, j];
                    prototype[i, j] = (prototype[i, j] > 255) ? 255 : prototype[i, j];

                    red = prototype[i, j] * 3 * ratioR[i, j];
                    red = (red > 255) ? 255 : red;

                    green = prototype[i, j] * 3 * ratioG[i, j];
                    green = (green > 255) ? 255 : green;

                    blue = prototype[i, j] * 3 * ratioB[i, j];
                    blue = (blue > 255) ? 255 : blue;

                    try
                    {
                        Color color = Color.FromArgb((int)red, (int)green, (int)blue);
                        //Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                    catch
                    {
                        Color color = Color.FromArgb((int)prototype[i, j], (int)prototype[i, j], (int)prototype[i, j]);
                        //MessageBox.Show(i + " " + j);
                        //MessageBox.Show(ratioR[i, j] + " " + ratioG[i, j] + " " + ratioB[i, j]);
                        imageFile.SetPixel(j, i, color);
                    }
                }
            }
            //complexR[j, i].r = (distance <= d0) ? complexR[j, i].r : 0;
            //complexR[j, i].i = (distance <= d0) ? complexR[j, i].i : 0;

            pictureBox3.Image = imageFile;
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.Equals("Ideal"))
            {
                IdealLowPassFFT();
            }
            else if (comboBox1.SelectedItem.Equals("Butterworth"))
            {
                ButterworthLowPassFFT();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button19_Click_1(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                pictureBox1.Image.Save(fileName.Substring(0, fileName.Length - 4) + "_fourier" + fileName.Substring(fileName.Length - 4, 4));
                MessageBox.Show(fileName.Substring(0, fileName.Length - 4) + "_fourier" + fileName.Substring(fileName.Length - 4, 4) + " has been successfully saved");
            }
            else
            {
                MessageBox.Show("No Fourier Image to be saved");
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            float salt = trackBar5.Value, pepper = trackBar6.Value, result;
            imageFile = new Bitmap(fileName, true);

            Random rng = new Random();

            for (int i = 0; i < imageFile.Height; i++)
            {
                for (int j = 0; j < imageFile.Width; j++)
                {
                    result = rng.Next(0, 99);
                    if (result < salt)
                    {
                        Color newColor = Color.FromArgb(255, 255, 255);
                        imageFile.SetPixel(j, i, newColor);
                    }
                    else
                    {
                        result = rng.Next(0, 99);
                        if (result < pepper)
                        {
                            Color newColor = Color.FromArgb(0, 0, 0);
                            imageFile.SetPixel(j, i, newColor);
                        }
                    }
                }
            }

            pictureBox3.Image = imageFile;
        }

        private void button23_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 1f;
            filter[0, 1] = 1f;
            filter[0, 2] = 1f;
            filter[1, 0] = 1f;
            filter[1, 1] = 1f;
            filter[1, 2] = 1f;
            filter[2, 0] = 1f;
            filter[2, 1] = 1f;
            filter[2, 2] = 1f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 1f, blue = 1f, green = 1f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red *= defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            green *= defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            blue *= defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                        }
                    }

                    red = (float)Math.Pow(red, 1.0f / (m * n));
                    green = (float)Math.Pow(green, 1.0f / (m * n));
                    blue = (float)Math.Pow(blue, 1.0f / (m * n));

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        private void button24_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 1f;
            filter[0, 1] = 1f;
            filter[0, 2] = 1f;
            filter[1, 0] = 1f;
            filter[1, 1] = 1f;
            filter[1, 2] = 1f;
            filter[2, 0] = 1f;
            filter[2, 1] = 1f;
            filter[2, 2] = 1f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += (1.0f / defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter]);
                            green += (1.0f / defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter]);
                            blue += (1.0f / defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter]);
                        }
                    }

                    red = (m * n) / red;
                    green = (m * n) / green;
                    blue = (m * n) / blue;

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }

        private void trackBar5_Scroll(object sender, EventArgs e)
        {

        }

        private void button25_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            int q = (int)numericUpDown1.Value;
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 1f;
            filter[0, 1] = 1f;
            filter[0, 2] = 1f;
            filter[1, 0] = 1f;
            filter[1, 1] = 1f;
            filter[1, 2] = 1f;
            filter[2, 0] = 1f;
            filter[2, 1] = 1f;
            filter[2, 2] = 1f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float red = 0f, blue = 0f, green = 0f;
                    float redd = 0f, blued = 0f, greend = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            red += (float)Math.Pow(defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter], (q + 1));
                            green += (float)Math.Pow(defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter], (q + 1));
                            blue += (float)Math.Pow(defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter], (q + 1));

                            redd += (float)Math.Pow(defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter], (q));
                            greend += (float)Math.Pow(defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter], (q));
                            blued += (float)Math.Pow(defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter], (q));
                        }
                    }

                    try
                    {
                        red /= redd;
                        green /= greend;
                        blue /= blued;

                        Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                        imageFile.SetPixel(x, y, newColor);
                    }
                    catch
                    {
                        //MessageBox.Show(green + " " + greend);
                    }
                }
            }
            pictureBox3.Image = imageFile;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        int GetMin(int a, int b, int c)
        {
            int temp;
            List<int> arr = new List<int>();
            arr.Add(a);
            arr.Add(b);
            arr.Add(c);
            for (int i = 0; i < arr.Count; i++)
            {
                for (int j = i; j < arr.Count; j++)
                {
                    if (arr[j] < arr[i])
                    {
                        temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }

            return arr[0];
        }

        private void button26_Click(object sender, EventArgs e)
        {
            Bitmap intensityFile = new Bitmap(fileName, true);
            Bitmap hueFile = new Bitmap(fileName, true);
            imageFile = new Bitmap(fileName, true);

            List<List<float>> hue = new List<List<float>>();
            List<List<float>> sat = new List<List<float>>();
            List<List<float>> intensity = new List<List<float>>();
            float num, denum, result;
            float red, green, blue;
            for (int i = 0; i < imageFile.Height; i++)
            {
                hue.Add(new List<float>());
                sat.Add(new List<float>());
                intensity.Add(new List<float>());
                for (int j = 0; j < imageFile.Width; j++)
                {
                    red = imageFile.GetPixel(j, i).R;
                    green = imageFile.GetPixel(j, i).G;
                    blue = imageFile.GetPixel(j, i).B;
                    num = (0.5f) * ((red - green) + (red - blue));
                    denum = (float)Math.Pow(Math.Pow((red - green), 2.0) + ((red - blue) * (green - blue)), 0.5f);
                    result = (denum == 0) ? (11.0f / 7.0f) : (float)Math.Acos(num / denum);
                    if (blue <= green)
                    {
                        //show += result + "\n";
                        hue[i].Add(result);
                    }
                    else
                    {
                        //show += (44.0f / 7.0f) - result + "\n";
                        hue[i].Add((44.0f / 7.0f) - result);
                    }
                    //MessageBox.Show("" + result);
                    result = (GetMin((int)red, (int)green, (int)blue) == 0) ? 0.0f : (1.0f - ((3.0f * (float)GetMin((int)red, (int)green, (int)blue)) / (red + green + blue)));
                    //result = ((red + green + blue) == 0) ? 1.0f : (1.0f - ((3.0f * (float)GetMin((int)red, (int)green, (int)blue)) / (red + green + blue)));
                    sat[i].Add(result);

                    result = (red + green + blue) / 3.0f;
                    intensity[i].Add(result);
                    try
                    {
                        hueFile.SetPixel(j, i, Color.FromArgb((int)red, (int)green, (int)blue));
                    }
                    catch
                    {
                        MessageBox.Show(red + " " + green + " " + blue);
                        MessageBox.Show(i + " " + j);
                    }
                }
            }

            //MessageBox.Show(show);

            float normalized;

            for (int i = 0; i < hue.Count; i++)
            {
                for (int j = 0; j < hue[i].Count; j++)
                {
                    if (hue[i][j] <= (22.0f / 7.0f))
                    {
                        hue[i][j] += (22.0f / 7.0f);
                    }
                    else
                    {
                        hue[i][j] -= (22.0f / 7.0f);
                    }

                    normalized = (hue[i][j] / (44.0f / 7.0f)) * 255.0f;
                    try
                    {
                        hueFile.SetPixel(j, i, Color.FromArgb((int)normalized, (int)normalized, (int)normalized));
                    }
                    catch
                    {
                        //MessageBox.Show(hue[i][j] + "");
                        //MessageBox.Show(normalized + "");
                        //MessageBox.Show(i + " " + j);
                    }
                    intensity[i][j] = (255 - intensity[i][j]);
                    normalized = intensity[i][j];
                    intensityFile.SetPixel(j, i, Color.FromArgb((int)normalized, (int)normalized, (int)normalized));
                }
            }

            pictureBox1.Image = hueFile;
            pictureBox4.Image = intensityFile;

            float maxRed = -1, maxGreen = -1, maxBlue = -1;
            float tempHue;
            for (int i = 0; i < imageFile.Height; i++)
            {
                for (int j = 0; j < imageFile.Width; j++)
                {
                    red = imageFile.GetPixel(j, i).R;
                    green = imageFile.GetPixel(j, i).G;
                    blue = imageFile.GetPixel(j, i).B;
                    if (hue[i][j] < (44.0f / 21.0f))
                    {
                        tempHue = hue[i][j];
                        blue = intensity[i][j] * (1.0f - sat[i][j]);
                        red = intensity[i][j] * (float)(1.0f + (sat[i][j] * Math.Cos(tempHue)) / (Math.Cos((22.0f / 21.0f) - tempHue)));
                        green = 3.0f * intensity[i][j] - (red + blue);
                    }
                    else if (hue[i][j] < (88.0f / 21.0f))
                    {
                        tempHue = hue[i][j] - (44.0f / 21.0f);
                        red = intensity[i][j] * (1.0f - sat[i][j]);
                        green = intensity[i][j] * (float)(1.0f + (sat[i][j] * Math.Cos(tempHue)) / (Math.Cos((22.0f / 21.0f) - tempHue)));
                        blue = 3.0f * intensity[i][j] - (red + green);
                    }
                    else
                    {
                        tempHue = hue[i][j] - (88.0f / 21.0f);
                        green = intensity[i][j] * (1.0f - sat[i][j]);
                        blue = intensity[i][j] * (float)(1.0f + (sat[i][j] * Math.Cos(tempHue)) / (Math.Cos((22.0f / 21.0f) - tempHue)));
                        red = 3.0f * intensity[i][j] - (green + blue);
                    }

                    if (maxRed == -1)
                    {
                        maxRed = red;
                    }
                    else
                    {
                        if (red > maxRed)
                        {
                            maxRed = red;
                        }
                    }

                    if (maxGreen == -1)
                    {
                        maxGreen = green;
                    }
                    else
                    {
                        if (green > maxGreen)
                        {
                            maxGreen = green;
                        }
                    }

                    if (maxBlue == -1)
                    {
                        maxBlue = blue;
                    }
                    else
                    {
                        if (blue > maxBlue)
                        {
                            maxBlue = blue;
                        }
                    }
                }
            }

            //MessageBox.Show(maxRed + " " + maxGreen + " " + maxBlue);

            for (int i = 0; i < imageFile.Height; i++)
            {
                for (int j = 0; j < imageFile.Width; j++)
                {
                    red = imageFile.GetPixel(j, i).R;
                    green = imageFile.GetPixel(j, i).G;
                    blue = imageFile.GetPixel(j, i).B;
                    if (hue[i][j] < (44.0f / 21.0f))
                    {
                        blue = intensity[i][j] * (1.0f - sat[i][j]);
                        red = intensity[i][j] * (float)(1.0f + (sat[i][j] * Math.Cos(hue[i][j])) / (Math.Cos((22.0f / 21.0f) - hue[i][j])));
                        green = 3.0f * intensity[i][j] - (red + blue);
                    }
                    else if (hue[i][j] < (88.0f / 21.0f))
                    {
                        hue[i][j] -= (44.0f / 21.0f);
                        red = intensity[i][j] * (1.0f - sat[i][j]);
                        green = intensity[i][j] * (float)(1.0f + (sat[i][j] * Math.Cos(hue[i][j])) / (Math.Cos((22.0f / 21.0f) - hue[i][j])));
                        blue = 3.0f * intensity[i][j] - (red + green);
                    }
                    else
                    {
                        hue[i][j] -= (88.0f / 21.0f);
                        green = intensity[i][j] * (1.0f - sat[i][j]);
                        blue = intensity[i][j] * (float)(1.0f + (sat[i][j] * Math.Cos(hue[i][j])) / (Math.Cos((22.0f / 21.0f) - hue[i][j])));
                        red = 3.0f * intensity[i][j] - (green + blue);
                    }

                    //red = (red < 0) ? 0 : red;
                    //green = (green < 0) ? 0 : green;
                    //blue = (blue < 0) ? 0 : blue;

                    red = (red > 255) ? 255 : red;
                    green = (green > 255) ? 255 : green;
                    blue = (blue > 255) ? 255 : blue;

                    //red = (red) * 255.0f / (maxRed);
                    //green = (green) * 255.0f / (maxGreen);
                    //blue = (blue) * 255.0f / (maxBlue);

                    try
                    {
                        Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                        imageFile.SetPixel(j, i, newColor);
                    }
                    catch
                    {
                        MessageBox.Show(((red * 255.0f) / maxRed) + " " + maxRed + " " + red);
                    }
                }
                pictureBox3.Image = imageFile;
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            imageFile = new Bitmap(fileName, true);
            m = 3; n = 3;

            aFilter = (m - 1) / 2; bFilter = (n - 1) / 2;

            filter = new float[m, n];

            redFilter = new float[imageFile.Width, imageFile.Height];
            greenFilter = new float[imageFile.Width, imageFile.Height];
            blueFilter = new float[imageFile.Width, imageFile.Height];

            filter[0, 0] = 1f;
            filter[0, 1] = 1f;
            filter[0, 2] = 1f;
            filter[1, 0] = 1f;
            filter[1, 1] = 1f;
            filter[1, 2] = 1f;
            filter[2, 0] = 1f;
            filter[2, 1] = 1f;
            filter[2, 2] = 1f;

            for (int x = aFilter; x < imageFile.Width - aFilter; x++)
            {
                for (int y = bFilter; y < imageFile.Height - bFilter; y++)
                {
                    float maxRed = -1f, minRed = -1f, maxGreen = -1f, minGreen = -1f, maxBlue = -1f, minBlue = -1f;
                    float red = 0f, blue = 0f, green = 0f;
                    for (int i = -aFilter; i <= aFilter; i++)
                    {
                        for (int j = -bFilter; j <= bFilter; j++)
                        {
                            if (maxRed == -1f)
                            {
                                maxRed = defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            }
                            else
                            {
                                if (defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter] > maxRed)
                                {
                                    maxRed = defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                                }
                            }

                            if (minRed == -1f)
                            {
                                minRed = defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                            }
                            else
                            {
                                if (defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter] < minRed)
                                {
                                    minRed = defaultFile.GetPixel(x + j, y + i).R * filter[i + aFilter, j + bFilter];
                                }
                            }

                            if (maxGreen == -1f)
                            {
                                maxGreen = defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            }
                            else
                            {
                                if (defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter] > maxGreen)
                                {
                                    maxGreen = defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                                }
                            }

                            if (minGreen == -1f)
                            {
                                minGreen = defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                            }
                            else
                            {
                                if (defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter] < minGreen)
                                {
                                    minGreen = defaultFile.GetPixel(x + j, y + i).G * filter[i + aFilter, j + bFilter];
                                }
                            }

                            if (maxBlue == -1f)
                            {
                                maxBlue = defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                            }
                            else
                            {
                                if (defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter] > maxBlue)
                                {
                                    maxBlue = defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                                }
                            }

                            if (minBlue == -1f)
                            {
                                minBlue = defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                            }
                            else
                            {
                                if (defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter] < minBlue)
                                {
                                    minBlue = defaultFile.GetPixel(x + j, y + i).B * filter[i + aFilter, j + bFilter];
                                }
                            }
                        }
                    }

                    red = (0.5f) * (maxRed + minRed);
                    green = (0.5f) * (maxGreen + minGreen);
                    blue = (0.5f) * (maxBlue + minBlue);

                    Color newColor = Color.FromArgb((int)red, (int)green, (int)blue);
                    imageFile.SetPixel(x, y, newColor);
                }
            }
            pictureBox3.Image = imageFile;
        }
    }
}